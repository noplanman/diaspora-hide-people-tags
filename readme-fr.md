Diaspora hide people and tags
=============================

## Pourquoi

Diaspora* permet de bloquer des utilisateurs/utilisatrices, mais dans ce cas, la personne bloquée ne peut plus agir sur vos posts. Il se peut que vous vouliez simplement ne plus lire ses posts parce qu'ils ne vous intéressent pas (en suivant un tag populaire par exemple), sans pour autant l'empêcher de commenter vos posts.

Diaspora* permet aussi de suivre des tags, mais pas d'empêcher de voir des posts avec des tags particulier : suivre #Linux par exemple, mais ne pas voir le post si le tag #german est présent aussi. Vous pouvez aussi masquer les posts tagués #nsfw par exemple.

## Installation

* installez l'extension [GreaseMonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/) ou [Scriptish](https://addons.mozilla.org/firefox/addon/scriptish) ou autre
* Cliquez [ce lien](https://framagit.org/spf/diaspora-hide-people-tags/raw/master/diaspora-hide-people-tags.user.js)
* Cliquez ``Installer``
* Accédez à votre compte

## Mon pod n'est ni diaspora-fr.org ni framasphere.org

J'ai fait ce script pour ces deux pods, mais vous pouvez l'adapter et ajouter d'autres pods. Pour installer ce script pour **votre** pod :

* allez dans les préférences de votre navigateur (pour Firefox ``about:addons``)
* ``Scripts utilisateur``
* cliquez sur ``Préférences`` en face du nom du script
* ajoutez votre pod dans **Matched pages** (n'oubliez pas d'ajouter ``*`` à la fin : ``https://mon-pod/*``) et ``https://mon-pod/people/*`` dans ``Pages exclues``
* éventuellement, désactivé/réactivé l'extension

## Comment ajouter des utilisateurs/trices et/ou tags

* allez dans les préférences de votre navigateur (pour Firefox ``about:addons``)
* ``Scripts utilisateur``
* cliquez sur ``Préférences`` en face du nom du script
* cliquez ``Modifier ce script``
* copiez l'identifiant de la personne (la série de chiffres et lettres juste après ``/people/``)
* collez cet identifiant dans le tableau **HideMe** :

````
var HideMe = [
  'id',
];
````

* enregistrez (``ctrl + s `` pour Firefox)

## exemple

Vous voulez cacher mes posts (euh, c'est juste pour l'exemple, hein ?!) et le tag #german :

````
var HideMe = [
  'a67fe237dabfccac', // mon id (url: https://diaspora-fr.org/people/a67fe237dabfccac)
  'german',
  'AutreID',
  'autreTag',
];
````

**N'oubliez pas la virgule à la fin de chaque ligne**.

## Comment fonctionne le script

Le script fonctionne lorsque vous *scrollez* (avec la souris ou les flèches), en vérifiant si il trouve le/les *id* ou tags dans la page, et cache les posts correspondant.

## FAQ (à vrai dire, personne n'a demandé)

* **Q** : pourquoi utiliser le *scroll* et ne pas attendre que la page se charge ?
* **A** : diaspora* utilise Ajax pour charger le flux. La page charge les éléments *statiques* **puis** le flux : mon script se chargerait avant, et ne fonctionnerait donc pas. La méthode du scroll n'est pas idéale, mais c'est celle qui fonctionne le mieux.

* **Q** : ce readme est-il bientôt fini ?
* **A** : oui.
