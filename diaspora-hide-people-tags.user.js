// ==UserScript==
// @name        diaspora* hide people and tags
// @namespace   diaspora-hide-people-tags
// @description Hide specific users posts and posts with specific tags
// @include     https://diaspora-fr.org/*
// @exclude     https://diaspora-fr.org/people/*
// @include     https://framasphere.org/*
// @exclude     https://framasphere.org/people/*
// @include     https://diaspote.org/*
// @exclude     https://diaspote.org/people/*
// @version     16-05-15
// @copyright   Public domain (http://unlicense.org/)
// @grant       none
// ==/UserScript==

$(document).ready().scroll(function(){
  var HideMe = [
    'people',
    'tag',
];

  $.each(HideMe, function(index, value) {
   $('a[href$="/' + value + '"]').parent(".media").parent(".stream_element").css({'display': 'none'});
  });
});
